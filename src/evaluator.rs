use crate::{
    ast::{
        Boolean, Expression, Identifier, IfExpression, InfixExpression, IntegerLiteral,
        PrefixExpression, Program, Statement,
    },
    environment::Environment,
    lexer::Token,
    object::Object,
};

pub fn eval(program: Program, environment: &mut Environment) -> Vec<Object> {
    let mut objects = vec![];
    for statement in program.statements {
        match statement {
            Statement::Let { value, name, .. } => {
                let expression = eval_expression(value, environment);
                environment.set(format!("{:?}", name), expression.clone());
                objects.push(expression);
            }
            Statement::Return { value, .. } => {
                return vec![eval_expression(value, environment)];
            }
            Statement::Expression { value, .. } => {
                let result = eval_expression(value, environment);
                println!("Evaluation of block statement: {result:?}");
                match result {
                    Object::Return { value } => {
                        return vec![*value];
                    }
                    Object::Error { message: _ } => {
                        return vec![result];
                    }
                    _ => {
                        objects.push(result);
                    }
                }
            }
            Statement::BlockStatement {
                statements: block_statements,
                ..
            } => {
                let result = eval_statements(block_statements, environment);
                println!("Evaluation of block statement: {result:?}");
                match result {
                    Object::Return { value } => {
                        return vec![*value];
                    }
                    Object::Error { message: _ } => {
                        return vec![result];
                    }
                    _ => {
                        objects.push(result);
                    }
                }
            }
        }
    }
    objects
}

fn eval_statements(statements: Vec<Statement>, environment: &mut Environment) -> Object {
    let mut x = None;
    for statement in statements {
        let replacement = match statement {
            Statement::Let { value, .. } => eval_expression(value, environment),
            Statement::Return { value, .. } => {
                println!("IT IS A RETURN");
                return Object::Return {
                    value: Box::new(eval_expression(value, environment)),
                };
            }
            Statement::Expression { value, .. } => {
                let result = eval_expression(value, environment);
                println!("Evaluation of block statement: {result:?}");
                match result {
                    Object::Return { value: _ } => {
                        return result;
                    }
                    Object::Error { message: _ } => {
                        return result;
                    }
                    _ => result,
                }
            }
            Statement::BlockStatement {
                statements: block_statements,
                ..
            } => {
                let result = eval_statements(block_statements, environment);
                match result {
                    Object::Return { value: _ } => {
                        return result;
                    }
                    Object::Error { message: _ } => {
                        return result;
                    }
                    _ => result,
                }
            }
        };
        x.replace(replacement);
    }
    x.unwrap_or(Object::Null)
}

fn eval_expression(expression: Expression, environment: &mut Environment) -> Object {
    match expression {
        Expression::Identifier(ident) => {
            let ident = format!("{ident:?}");
            match environment.get(&ident) {
                Some(val) => val.clone(),
                None => Object::Error { message: format!("Identifier not found: {ident}")}
            }
        },
        Expression::IntegerLiteral(IntegerLiteral { token: _, value }) => Object::Integer { value },
        Expression::Boolean(Boolean { token: _, value }) => Object::Boolean { value },
        Expression::PrefixExpression(prefix_expression) => {
            let PrefixExpression {
                operator, right, ..
            } = *prefix_expression; // Need to deref, because of Box<_>
            let right = eval_expression(right, environment);
            eval_prefix(operator, right, environment)
        }
        Expression::InfixExpression(infix_expression) => {
            let InfixExpression {
                operator,
                right,
                left,
                ..
            } = *infix_expression;
            let left = eval_expression(left, environment);
            let right = eval_expression(right, environment);
            eval_infix(operator, left, right, environment)
        }
        Expression::IfExpression(if_expression) => {
            let IfExpression {
                condition,
                consequence,
                alternative,
                ..
            } = *if_expression;
            eval_if(condition, consequence, alternative, environment)
        }
        Expression::FunctionLiteral(_) => todo!(),
        Expression::CallExpression(_) => todo!(),
    }
}

fn eval_if(
    condition: Expression,
    consequence: Statement,
    alternative: Option<Statement>,
    environment: &mut Environment,
) -> Object {
    let condition = eval_expression(condition, environment);
    if condition.truthy() {
        let consequence = eval_statements(vec![consequence], environment);
        println!("consequence: {consequence:?}");
        consequence
    } else if let Some(alternative) = alternative {
        eval_statements(vec![alternative], environment)
    } else {
        Object::Null
    }
}

fn eval_infix(
    operator: Token,
    left: Object,
    right: Object,
    environment: &mut Environment,
) -> Object {
    // println!("left: {left:?}, right: {right:?}, operator: {operator:?}");

    if matches!(right, Object::Null) || matches!(left, Object::Null) {
        return Object::Null;
    }

    if let (Object::Integer { value: left }, Object::Integer { value: right }) =
        (left.clone(), right.clone())
    {
        return eval_integer_infix(operator, left, right, environment);
    }

    let left = left.truthy();
    let right = right.truthy();

    match operator.clone() {
        Token::Equal => Object::Boolean {
            value: left == right,
        },
        Token::NotEqual => Object::Boolean {
            value: left != right,
        },
        _ => Object::Error {
            message: format!("unknown operator: {operator:?}{right:?}"),
        },
    }
}

fn eval_integer_infix(
    operator: Token,
    left: i64,
    right: i64,
    environment: &mut Environment,
) -> Object {
    println!("{left:?} {operator:?} {right:?}");
    match operator {
        Token::Plus => Object::Integer {
            value: left + right,
        },
        Token::Minus => Object::Integer {
            value: left - right,
        },
        Token::Slash => Object::Integer {
            value: left / right,
        },
        Token::Asterisk => Object::Integer {
            value: left * right,
        },
        Token::LChevron => Object::Boolean {
            value: left < right,
        },
        Token::RChevron => Object::Boolean {
            value: left > right,
        },
        Token::Equal => Object::Boolean {
            value: left == right,
        },
        Token::NotEqual => Object::Boolean {
            value: left != right,
        },
        _ => Object::Error {
            message: format!("unknown operator: {operator:?}{right:?}"),
        },
    }
}

fn eval_prefix(operator: Token, right: Object, environment: &mut Environment) -> Object {
    println!("{operator:?} {right:?}");
    match operator {
        Token::Bang => eval_bang_operator(right, environment),
        Token::Minus => eval_minus_operator(right, environment),
        _ => Object::Error {
            message: format!("unknown operator: {operator:?}{right:?}"),
        },
    }
}

fn eval_minus_operator(right: Object, environment: &mut Environment) -> Object {
    if let Object::Integer { value } = right {
        return Object::Integer { value: -value };
    }
    eprintln!("Illegal operation: using '-'-operator for type {}", right);
    Object::Null
}

fn eval_bang_operator(right: Object, environment: &mut Environment) -> Object {
    let value = match right {
        Object::Null => true,
        Object::Boolean { value } => !value,
        _ => false,
    };
    Object::Boolean { value }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{lexer::Lexer, object::Object, parser::Parser};

    #[test]
    fn test_bang_operator() {
        let inputs = [("!true", false), ("!false", true), ("!!false", false)];
        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            assert_eq!(evaluated.len(), 1);
            assert!(test_bool(evaluated[0].clone(), expected));
        }
    }

    #[test]
    fn test_eval_int() {
        let inputs = [
            ("5", 5),
            ("10", 10),
            ("-5", -5),
            ("-10", -10),
            ("5 + 5 + 5 + 5 - 10", 10),
            ("2 * 2 * 2 * 2 * 2", 32),
            ("-50 + 100 + -50", 0),
            ("5 * 2 + 10", 20),
            ("5 + 2 * 10", 25),
            ("20 + 2 * -10", 0),
            ("50 / 2 * 2 + 10", 60),
            ("2 * (5 + 10)", 30),
            ("3 * 3 * 3 + 10", 37),
            ("3 * (3 * 3) + 10", 37),
            ("(5 + 10 * 2 + 15 / 3) * 2 + -10", 50),
        ];
        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            assert_eq!(evaluated.len(), 1);
            assert!(test_int(evaluated[0].clone(), expected));
        }
    }

    #[test]
    fn test_eval_bool() {
        let inputs = [
            ("true", true),
            ("false", false),
            ("1 < 2", true),
            ("1 > 2", false),
            ("1 < 1", false),
            ("1 > 1", false),
            ("1 == 1", true),
            ("1 != 1", false),
            ("1 == 2", false),
            ("1 != 2", true),
            ("true == true", true),
            ("false == false", true),
            ("true == false", false),
            ("true != false", true),
            ("false != true", true),
            ("(1 < 2) == true", true),
            ("(1 < 2) == false", false),
            ("(1 > 2) == true", false),
            ("(1 > 2) == false", true),
        ];
        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            assert_eq!(evaluated.len(), 1);
            assert!(test_bool(evaluated[0].clone(), expected));
        }
    }

    #[test]
    fn test_conditionals() {
        let inputs = [
            ("if (true) { 10 }", Some(10)),
            ("if (false) { 10 }", None),
            ("if (1) { 10 }", Some(10)),
            ("if (1 < 2) { 10 }", Some(10)),
            ("if (1 > 2) { 10 }", None),
            ("if (1 > 2) { 10 } else { 20 }", Some(20)),
            ("if (1 < 2) { 10 } else { 20 }", Some(10)),
        ];

        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            assert_eq!(evaluated.len(), 1);
            if expected.is_some() {
                assert!(test_int(evaluated[0].clone(), expected.unwrap()));
            } else {
                assert!(matches!(evaluated[0].clone(), Object::Null));
            }
        }
    }

    #[test]
    fn test_return_statements() {
        let inputs = [
            ("return 10;", 10),
            ("return 10; 9;", 10),
            ("return 2 * 5; 9;", 10),
            ("9; return 2 * 5; 9;", 10),
        ];
        for (input, expected) in inputs {
            // println!("{input} -> {expected}");
            let evaluated = test_eval(input.into());
            println!("evaluated: {evaluated:?}");
            // assert_eq!(evaluated.len(), 1);
            assert!(test_int(evaluated[0].clone(), expected));
        }
    }

    #[test]
    fn how_stupid_is_my_impl() {
        let input = "if (10 > 1) {
      if (10 > 1) {
        return 10;
      }

      return 1;
    }";
        let evaluated = test_eval(input.into());
        assert_eq!(
            evaluated.len(),
            1,
            "Should only have 1 true return statement in there"
        );
        assert!(test_int(evaluated[0].clone(), 10));
    }

    // TODO: Fix me later plz
    /*
    #[test]
    fn test_error_handling() {
        let inputs = [
            ("5 + true", "type mismatch: INTEGER + BOOLEAN"),
            ("5 + true;5;", "type mismatch: INTEGER + BOOLEAN"),
        ];
        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            assert_eq!(evaluated.len(), 1);
            assert_eq!(format!("{}", evaluated[0]), expected);
        }
    }
    */

    #[test]
    fn test_let_statements() {
        let inputs = [
            ("let a = 5; a;", 5),
            ("let a = 5 * 5; a;", 25),
            ("let a = 5; let b = a; b;", 5),
            ("let a = 5; let b = a; let c = a + b + 5; c;", 15),
        ];

        for (input, expected) in inputs {
            let evaluated = test_eval(input.into());
            // assert_eq!(
            //     evaluated.len(),
            //     1,
            //     "Should only have 1 true return statement in there"
            // );
            assert!(test_int(evaluated.last().unwrap().clone(), expected));
        }
    }

    fn test_eval(input: String) -> Vec<Object> {
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);
        let program = parser.parse_program().expect("Should be parseable");
        let mut env = Environment::new();
        eval(program, &mut env)
    }

    fn test_int(value: Object, expected: i64) -> bool {
        println!("value: {value:?}, {expected}");
        matches!(value, Object::Integer { value: x } if x == expected)
    }

    fn test_bool(value: Object, expected: bool) -> bool {
        // println!("value: {value:?}, {expected}");
        matches!(value, Object::Boolean { value: x } if x == expected)
    }
}
