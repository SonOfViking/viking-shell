mod lexer;
mod ast;
mod parser;
mod object;
mod evaluator;
mod environment;

use anyhow::Result;

mod repl {
    use anyhow::Result;
    use std::io;
    use std::io::Write;

    use crate::{lexer::{Lexer, Token}, parser::{self, Parser}, evaluator, object::Object, environment::Environment};

    const PROMPT: &str = ">>";
    const MONKEY_FACE: &str = r#"                                            __,__
                                   .--.  .-"     "-.  .--.
                                  / .. \/  .-. .-.  \/ .. \
                                 | |  '|  /   Y   \  |'  | |
                                 | \   \  \ 0 | 0 /  /   / |
                                  \ '- ,\.-"""""""-./, -' /
                                   ''-' /_   ^ ^   _\ '-''
                                       |  \._   _./  |
                                       \   \ '~' /   /
                                        '._ '-=-' _.'
                                           '-----'
                                 Welcome to the monkey REPL
                                "#;

    pub fn start() -> Result<()> {
        println!("{}", MONKEY_FACE);
        let stdin = io::stdin();
        loop {
            let mut line = String::new();
            print!("{} ", PROMPT);
            std::io::stdout().flush().unwrap();
            if let Err(e) = stdin.read_line(&mut line) {
                println!("{e:?}");
                break;
            }
            let lexer = Lexer::new(line);
            let mut parser = Parser::new(lexer);
            let Some(program) = parser.parse_program() else {
                // Assume that parser.errors is not empty here
                for err in parser.errors {
                    println!("{:?}", err);
                }
                continue;
            };
            let mut env = Environment::new();
            let evaluated:Vec<_> = evaluator::eval(program, &mut env).iter().map(Object::inspect).collect();
            println!("{:?}", evaluated);
        }

        Ok(())
    }
}

fn main() -> Result<()> {
    println!("Welcome to the Monkey programming langauge!");
    repl::start()?;
    Ok(())
}
