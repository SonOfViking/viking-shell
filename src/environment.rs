use std::collections::hash_map::HashMap;

use crate::object::Object;

pub struct Environment {
    map: HashMap<String, Object>
}

impl Environment {
    pub fn new() -> Self {
        Self{
            map: HashMap::new(),
        }
    }

    pub fn get<S: AsRef<str>>(&self, name: S) -> Option<&Object> {
        self.map.get(name.as_ref())
    }

    pub fn set(&mut self, name: String, value: Object) {
        self.map.insert(name, value);
    }
}
