use crate::lexer::Token;

pub trait Node {
    fn token_literal(&self) -> Token;
}

#[derive(Debug, Clone)]
pub enum Statement {
    Let { token: Token, name: Identifier, value: Expression },
    Return { token: Token, value: Expression },
    Expression { token: Token, value: Expression },
    BlockStatement { token: Token, statements: Vec<Statement> },
}

impl Node for Statement {
    fn token_literal(&self) -> Token {
        match self {
            Statement::Let { token, ..} => token.clone(),
            Statement::Return { token, ..} => token.clone(),
            Statement::Expression { token, .. } => token.clone(),
            Statement::BlockStatement { token, .. } => token.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Expression {
    Identifier(Identifier),
    IntegerLiteral(IntegerLiteral),
    Boolean(Boolean),
    PrefixExpression(Box<PrefixExpression>),
    InfixExpression(Box<InfixExpression>),
    IfExpression(Box<IfExpression>),
    FunctionLiteral(Box<FunctionLiteral>),
    CallExpression(Box<CallExpression>),
}

impl Node for Expression {
    fn token_literal(&self) -> Token {
        match self {
            Expression::Identifier(ident) => ident.token.clone(),
            Expression::IntegerLiteral(int) => int.token.clone(),
            Expression::PrefixExpression(prefix) => prefix.token.clone(),
            Expression::InfixExpression(prefix) => prefix.token.clone(),
            Expression::Boolean(b) => b.token.clone(),
            Expression::IfExpression(if_expression) => if_expression.token.clone(),
            Expression::FunctionLiteral(func) => func.token.clone(),
            Expression::CallExpression(call) => call.token.clone(),
        }
    }
}

#[derive(Debug)]
pub struct Program {
    pub statements: Vec<Statement>,
}

impl Node for Program {
    fn token_literal(&self) -> Token {
        if self.statements.is_empty() {
            Token::EOF
        } else {
            self.statements[0].token_literal()
        }
    }
}

pub struct LetStatement {
    pub token: Token,
    pub name: Identifier,
    pub value: Expression,
}

impl Node for LetStatement {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}

#[derive(Debug, Clone)]
pub struct Identifier {
    pub token: Token,
}

#[derive(Debug, Clone)]
pub struct PrefixExpression {
    pub token: Token,
    pub operator: Token,
    pub right: Expression,
}

#[derive(Debug, Clone)]
pub struct InfixExpression {
    pub token: Token,
    pub operator: Token,
    pub right: Expression,
    pub left: Expression,
}

#[derive(Debug, Clone)]
pub struct IfExpression {
    pub token: Token,
    pub condition: Expression,
    pub consequence: Statement,
    pub alternative: Option<Statement>,
}

#[derive(Debug, Clone)]
pub struct FunctionLiteral {
    pub token: Token,
    pub parameters: Vec<Identifier>,
    pub body: Statement, // BlockStatement
}

#[derive(Debug, Clone)]
pub struct CallExpression {
    pub token: Token,
    pub function: Expression,
    pub arguments: Vec<Expression>,
}

impl Node for PrefixExpression {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}

impl Node for InfixExpression {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}

#[derive(Debug, Clone)]
pub struct IntegerLiteral {
    pub token: Token,
    pub value: i64,
}

#[derive(Debug, Clone)]
pub struct Boolean {
    pub token: Token,
    pub value: bool,
}

impl Node for Identifier {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}

impl Node for IntegerLiteral {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}

impl Node for Boolean {
    fn token_literal(&self) -> Token {
        self.token.clone()
    }
}
