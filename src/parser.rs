use std::collections::HashMap;

use anyhow::{anyhow, bail, Error, Result};

use crate::ast::{
    CallExpression, Expression, FunctionLiteral, Identifier, IfExpression, InfixExpression,
    IntegerLiteral, PrefixExpression, Program, Statement,
};
use crate::lexer::{Lexer, Token};

#[derive(Debug, Copy, Clone, Eq, PartialEq, PartialOrd)]
enum Precedence {
    Lowest,
    Equals,      // ==
    Lessgreater, // > or <
    Sum,         // +
    Product,     // *
    Prefix,      // -X or !X
    Call,        // myFunction(X)
}

const PRECEDENCES: &[(Token, Precedence)] = &[
    (Token::LParen, Precedence::Call),
    (Token::Equal, Precedence::Equals),
    (Token::NotEqual, Precedence::Equals),
    (Token::LChevron, Precedence::Lessgreater),
    (Token::RChevron, Precedence::Lessgreater),
    (Token::Plus, Precedence::Sum),
    (Token::Minus, Precedence::Sum),
    (Token::Asterisk, Precedence::Product),
    (Token::Slash, Precedence::Product),
];

#[derive(Debug)]
pub struct Parser {
    lexer: Lexer,
    current_token: Token,
    peek_token: Token,
    pub errors: Vec<Error>,
}

impl Parser {
    pub fn new(lexer: Lexer) -> Parser {
        let mut parser = Parser {
            lexer,
            current_token: Token::EOF,
            peek_token: Token::EOF,
            errors: Vec::new(),
        };

        parser.next_token();
        parser.next_token();

        parser
    }

    fn peek_precedence(&self) -> Precedence {
        PRECEDENCES
            .into_iter()
            .find(|&(token, _)| self.peek_token == *token)
            .map(|(_, precedence)| precedence.clone())
            .unwrap_or(Precedence::Lowest)
    }

    fn current_precedence(&self) -> Precedence {
        PRECEDENCES
            .into_iter()
            .find(|&(token, _)| self.current_token == *token)
            .map(|(_, precedence)| precedence.clone())
            .unwrap_or(Precedence::Lowest)
    }

    fn parse_identifier(&self) -> Expression {
        Expression::Identifier(Identifier {
            token: self.current_token.clone(),
        })
    }

    fn next_token(&mut self) {
        self.current_token = self.peek_token.clone();
        self.peek_token = self.lexer.next_token();
    }

    pub fn parse_program(&mut self) -> Option<Program> {
        let mut program = Program { statements: vec![] };
        while self.current_token != Token::EOF {
            if let Some(statement) = self.parse_statement() {
                program.statements.push(statement);
            }
            self.next_token();
        }
        if !self.errors.is_empty() {
            //println!("Got unexpected errors: {:?}", self.errors);
            return None;
        }
        Some(program)
    }

    fn parse_statement(&mut self) -> Option<Statement> {
        match self.current_token {
            Token::Let => self.parse_let_statement(),
            Token::Return => self.parse_return_statement(),
            _ => {
                let expr_stat = self.parse_expression_statement();
                expr_stat
            }
        }
    }

    fn parse_let_statement(&mut self) -> Option<Statement> {
        let token = self.current_token.clone();

        if matches!(self.peek_token, Token::Ident(_)) {
            self.next_token();
        } else {
            self.errors.push(anyhow!(
                "Expected next token to be {:?}, got {:?} instead",
                token,
                self.peek_token
            ));
            return None;
        }

        let name = Identifier {
            token: self.current_token.clone(),
        };

        self.advance_if_peek(Token::Assign)?;

        self.next_token();

        let value = self.parse_expression(Precedence::Lowest)?;

        if matches!(self.peek_token, Token::Semicolon) {
            self.next_token();
        }

        Some(Statement::Let { token, name, value })
    }

    fn parse_return_statement(&mut self) -> Option<Statement> {
        let token = self.current_token.clone();

        if !matches!(token, Token::Return) {
            self.errors.push(anyhow!("expected return token"));
            return None;
        }
        self.next_token();

        let value = self.parse_expression(Precedence::Lowest)?;

        if matches!(self.peek_token, Token::Semicolon) {
            self.next_token();
        }

        Some(Statement::Return { token, value })
    }

    fn parse_expression_statement(&mut self) -> Option<Statement> {
        let token = self.current_token.clone();
        let expression = self.parse_expression(Precedence::Lowest)?;

        if self.peek_token == Token::Semicolon {
            self.next_token();
        }
        let statement = Statement::Expression {
            token,
            value: expression,
        };
        Some(statement)
    }

    fn parse_integer_literal(&mut self, int: String) -> Option<Expression> {
        let token = self.current_token.clone();
        match int.parse() {
            Ok(int) => {
                let literal = IntegerLiteral { token, value: int };
                Some(Expression::IntegerLiteral(literal))
            }
            Err(e) => {
                self.errors.push(anyhow!(e));
                None
            }
        }
    }

    fn parse_expression(&mut self, precedence: Precedence) -> Option<Expression> {
        let mut left = match self.current_token.clone() {
            // Token::Illegal => todo!(),
            Token::Ident(_) => Some(self.parse_identifier()),
            Token::Integer(int) => self.parse_integer_literal(int),
            // Token::Assign => todo!(),
            Token::Bang => self.parse_prefix_expression(),
            Token::Minus => self.parse_prefix_expression(),
            // Token::Comma => todo!(),
            Token::LParen => self.parse_group_expression(),
            // Token::RParen => todo!(),
            // Token::LBrace => todo!(),
            // Token::RBrace => todo!(),
            Token::Function => self.parse_function_literal(),
            // Token::Let => todo!(),
            Token::If => self.parse_if_expression(),
            // Token::Else => self.pars,
            Token::True => self.parse_boolean(),
            Token::False => self.parse_boolean(),
            _ => None,
        };

        if left.is_none() {
            self.errors.push(anyhow!(
                "No prefix expression parsing rule for {:?}",
                self.current_token
            ));
            return None;
        }

        while !matches!(self.peek_token, Token::Semicolon) && precedence < self.peek_precedence() {
            if PRECEDENCES
                .iter()
                .find(|(token, _)| token == &self.peek_token.clone())
                .is_none()
            {
                return left;
            }

            self.next_token();

            let right = match self.current_token.clone() {
                // Token::Ident(_) => Some(self.parse_identifier()),
                // Token::Integer(int) => self.parse_integer_literal(int),
                // Token::Bang => self.parse_prefix_expression(),
                Token::Minus => self.parse_infix_expression(left.clone().unwrap()),
                Token::Plus => self.parse_infix_expression(left.clone().unwrap()),
                Token::Slash => self.parse_infix_expression(left.clone().unwrap()),
                Token::Asterisk => self.parse_infix_expression(left.clone().unwrap()),
                Token::LChevron => self.parse_infix_expression(left.clone().unwrap()),
                Token::RChevron => self.parse_infix_expression(left.clone().unwrap()),
                Token::Equal => self.parse_infix_expression(left.clone().unwrap()),
                Token::NotEqual => self.parse_infix_expression(left.clone().unwrap()),
                Token::LParen => self.parse_call_expression(left.clone().unwrap()),
                _ => None,
            };
            left = right;
        }

        left
    }

    fn parse_infix_expression(&mut self, left: Expression) -> Option<Expression> {
        let token = self.current_token.clone();
        let precedence = self.current_precedence();
        self.next_token();
        let right = self.parse_expression(precedence)?;
        let exp = InfixExpression {
            operator: token.clone(),
            token,
            left,
            right,
        };

        Some(Expression::InfixExpression(Box::new(exp)))
    }

    fn parse_prefix_expression(&mut self) -> Option<Expression> {
        let token = self.current_token.clone();
        self.next_token();
        let right = self.parse_expression(Precedence::Prefix)?;
        let exp = PrefixExpression {
            operator: token.clone(),
            token,
            right,
        };

        Some(Expression::PrefixExpression(Box::new(exp)))
    }

    fn parse_boolean(&mut self) -> Option<Expression> {
        let token = self.current_token.clone();
        // self.next_token();
        Some(Expression::Boolean(crate::ast::Boolean {
            value: matches!(token.clone(), Token::True),
            token,
        }))
    }

    fn parse_group_expression(&mut self) -> Option<Expression> {
        self.next_token();
        let exp = self.parse_expression(Precedence::Lowest);
        if self.peek_token != Token::RParen {
            return None;
        }
        self.next_token();
        exp
    }

    fn parse_if_expression(&mut self) -> Option<Expression> {
        let token = self.current_token.clone();

        if !self.expect_peek(Token::LParen) {
            return None;
        }

        self.next_token();

        let Some(condition) = self.parse_expression(Precedence::Lowest) else {
            self.errors.push(anyhow!("Could not parse expression for if statement"));
            return None;
        };

        if !self.expect_peek(Token::RParen) {
            return None;
        }

        if !self.expect_peek(Token::LBrace) {
            return None;
        }

        let consequence = self.parse_block_statement();

        let alternative = if let Token::Else = self.peek_token {
            self.next_token();
            if !self.expect_peek(Token::LBrace) {
                return None;
            }
            Some(self.parse_block_statement())
        } else {
            None
        };

        let expression = IfExpression {
            token,
            condition,
            consequence,
            alternative,
        };

        Some(Expression::IfExpression(Box::new(expression)))
    }

    fn expect_peek(&mut self, token: Token) -> bool {
        if matches!(self.peek_token.clone(), token) {
            self.next_token();
            true
        } else {
            self.errors.push(anyhow!(
                "Expected next token to be {:?}, got {:?} instead",
                token,
                self.peek_token
            ));
            false
        }
    }

    fn advance_if_peek(&mut self, token: Token) -> Option<()> {
        let peek = self.peek_token.clone();
        if matches!(peek, token) {
            self.next_token();
            Some(())
        } else {
            self.errors.push(anyhow!(
                "Expected next token to be {:?}, got {:?} instead",
                token,
                self.peek_token
            ));
            None
        }
    }

    fn parse_block_statement(&mut self) -> Statement {
        let token = self.current_token.clone();
        let mut statements = vec![];

        self.next_token();
        while !matches!(self.current_token, Token::RBrace)
            && !matches!(self.current_token, Token::EOF)
        {
            if let Some(statement) = self.parse_statement() {
                statements.push(statement);
            }
            self.next_token();
        }

        Statement::BlockStatement { token, statements }
    }

    fn parse_function_literal(&mut self) -> Option<Expression> {
        let token = self.current_token.clone();

        self.advance_if_peek(Token::LParen)?;

        let parameters = self.parse_function_params()?;

        self.advance_if_peek(Token::LBrace)?;

        let body = self.parse_block_statement();

        Some(Expression::FunctionLiteral(Box::new(FunctionLiteral {
            token,
            parameters,
            body,
        })))
    }

    fn parse_function_params(&mut self) -> Option<Vec<Identifier>> {
        let mut idents = vec![];

        if matches!(self.peek_token, Token::RParen) {
            self.next_token();
            return Some(idents);
        }

        self.next_token();

        idents.push(Identifier {
            token: self.current_token.clone(),
        });

        while matches!(self.peek_token, Token::Comma) {
            self.next_token();
            self.next_token();
            idents.push(Identifier {
                token: self.current_token.clone(),
            });
        }

        self.advance_if_peek(Token::RParen)?;

        Some(idents)
    }

    fn parse_call_expression(&mut self, function: Expression) -> Option<Expression> {
        let token = self.current_token.clone();
        let arguments = self.parse_call_arguments()?;

        Some(Expression::CallExpression(Box::new(CallExpression {
            token,
            function,
            arguments,
        })))
    }

    fn parse_call_arguments(&mut self) -> Option<Vec<Expression>> {
        let mut arguments = vec![];

        if matches!(self.peek_token, Token::RParen) {
            self.next_token();
            return Some(arguments);
        }

        self.next_token();

        arguments.push(self.parse_expression(Precedence::Lowest)?);

        while matches!(self.peek_token, Token::Comma) {
            self.next_token();
            self.next_token();
            arguments.push(self.parse_expression(Precedence::Lowest)?);
        }

        self.advance_if_peek(Token::RParen)?;

        Some(arguments)
    }
}

fn check_parser_errors() {
    todo!("not implemented");
}

#[cfg(test)]
mod tests {
    use crate::{
        ast::{Expression, LetStatement, Node, Statement},
        lexer::{Lexer, Token},
        parser::Precedence,
    };

    use super::Parser;
    use anyhow::{bail, Result};

    #[test]
    fn test_let_statements() -> Result<()> {
        let input = "let x = 5;
        let y = 10;
        let foobar = 838383;
        "
        //         let input = "let x 5;
        // let = 10;
        // let 838383;
        // "
        .into();
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();

        let tests = vec!["x", "y", "foobar"];

        assert_eq!(program.statements.len(), tests.len());

        for (tt, statement) in tests.iter().zip(program.statements.iter()) {
            assert_eq!(statement.token_literal(), Token::Let);
            if let Statement::Let {
                token: _,
                name,
                value: _,
            } = statement
            {
                assert_eq!(name.token_literal(), Token::Ident(tt.to_string()));
            }
        }

        Ok(())
    }

    #[test]
    fn test_return_statements() -> Result<()> {
        let input = "return 5;
return 10;
return 993322;
"
        .into();
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert!(parser.errors.is_empty());

        assert_eq!(program.statements.len(), 3);

        for statement in program.statements {
            assert_eq!(statement.token_literal(), Token::Return);
            assert!(matches!(statement, Statement::Return { .. }));
        }
        Ok(())
    }
    #[test]
    fn test_expression_statements() -> Result<()> {
        let input = "foobar".into();

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);
        let program = parser.parse_program().unwrap();
        // checkParserErrors(t, p)

        assert!(program.statements.len() == 1);
        assert!(matches!(
            &program.statements[0],
            Statement::Expression {
                token: Token::Ident(_),
                value: Expression::Identifier(_),
            }
        ));
        Ok(())
    }

    #[test]
    fn test_integer_literal_statements() -> Result<()> {
        let input = "5;".into();

        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);
        let program = parser.parse_program().unwrap();
        // checkParserErrors(t, p)

        assert!(program.statements.len() == 1);
        assert!(matches!(
            &program.statements[0],
            Statement::Expression {
                token: Token::Integer(_),
                value: Expression::IntegerLiteral(_),
            }
        ));
        Ok(())
    }

    #[test]
    fn test_parsing_prefix_expressions() -> Result<()> {
        let input = ["!5", "-15"];
        for i in input {
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            assert_eq!(
                program.statements.len(),
                1,
                "Expected program to only have 1 statement"
            );
            assert!(matches!(
                &program.statements[0],
                Statement::Expression {
                    token: _,
                    value: Expression::PrefixExpression(_),
                }
            ));
        }
        Ok(())
    }

    #[test]
    fn test_parsing_infix_expressions() -> Result<()> {
        let input = [
            "5 + 5;", "5 - 5;", "5 * 5;", "5 / 5;", "5 > 5;", "5 < 5;", "5 == 5;", "5 != 5;",
        ];

        for i in input {
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());

            assert_eq!(
                program.statements.len(),
                1,
                "Expected program to only have 1 statement"
            );
            assert!(matches!(
                &program.statements[0],
                Statement::Expression {
                    token: _,
                    value: Expression::InfixExpression(_),
                }
            ));
        }
        Ok(())
    }

    #[test]
    fn test_operator_precedence() {
        let input = [
            "-a * b",
            "!-a",
            "a + b + c",
            "a + b - c",
            "a * b * c",
            "a * b / c",
            "a + b / c",
            "a + b * c + d / e - f",
            "3 + 4; -5 * 5",
            "5 > 4 == 3 < 4",
            "5 < 4 != 3 > 4",
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "3 > 5 == false",
            "3 < 5 == false",
            "5 * 2 + 10",
        ];

        for i in input {
            println!("{i}");
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            // println!("{:#?}", program);

            // actual := program.String()
            // if actual != tt.expected {
            //     t.Errorf("expected=%q, got=%q", tt.expected, actual)
            // }
        }
    }

    #[test]
    fn test_grouped_expressions() {
        let input = ["(a + b)", "1 + (2 + 3) + 4", "(a + b) * c + d / e - f"];
        for i in input {
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            // println!("{:?}", program);

            // actual := program.String()
            // if actual != tt.expected {
            //     t.Errorf("expected=%q, got=%q", tt.expected, actual)
            // }
        }
    }

    #[test]
    fn test_boolean() {
        let input = [
            "true;",
            "false;",
            "let foobar = true;",
            "let foobar = false;",
        ];

        for i in input {
            println!("{i:?}");
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
        }
    }

    #[test]
    fn test_if_statment() {
        let input = ["if (x < y { x }", "if (x < y) { x } else { y }"];
        for i in input {
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            assert_eq!(program.statements.len(), 1);
        }
    }

    #[test]
    fn test_fn_statment() {
        let input = [
            "fn(x, y) { x + y; }",
            "fn() {}",
            "fn(x) {}",
            "fn(x, y, z) {}",
        ];
        for i in input {
            println!("{i}");
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            assert_eq!(program.statements.len(), 1);
        }
    }

    #[test]
    fn test_call_expression() {
        let input = [
            "add(2, 3)",
            "add(2 + 1, 6 * 9 * 4 * 2 * 0)",
            "add((((a + b) + ((c * d) / f)) + g))",
            "add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))",
        ];
        for i in input {
            println!("{i}");
            let lexer = Lexer::new(i.into());
            let mut parser = Parser::new(lexer);
            let program = parser.parse_program().expect("Program should be parsable");
            assert!(parser.errors.is_empty());
            assert_eq!(program.statements.len(), 1);
        }
    }

    #[test]
    fn test_precedence() {
        assert!(Precedence::Lowest < Precedence::Product);
    }
}
