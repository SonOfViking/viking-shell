use crate::ast::{Node, Statement};

#[derive(Debug, Clone)]
pub enum Object {
    Integer { value: i64 },
    Boolean { value: bool },
    Return { value: Box<Object> },
    Error { message: String },
    Null,
}

impl std::fmt::Display for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Integer { value } => write!(f, "{value}")?,
            Self::Boolean { value } => write!(f, "{value}")?,
            Self::Return { value } => write!(f, "{}", *value)?,
            Self::Null => write!(f, "NULL")?,
            Self::Error { message } => write!(f, "ERROR: {}", message)?,
        }
        Ok(())
    }
}

impl Object {
    pub fn inspect(&self) -> String {
        format!("{}", self)
    }

    pub fn truthy(&self) -> bool {
        match self {
            Object::Integer { value } => *value != 0,
            Object::Boolean { value } => *value,
            Object::Null => false,
            _ => true,
        }
    }
}
