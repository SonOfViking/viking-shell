#[derive(Eq, PartialEq, Debug, Clone, Hash)]
pub enum Token {
    Illegal,
    EOF,
    // Identifiers + literals
    Ident(String),
    Integer(String),
    // Operators
    Assign,
    Plus,
    Bang,
    Minus,
    Slash,
    Asterisk,
    LChevron,
    RChevron,
    Equal,
    NotEqual,
    // Delimiters
    Comma,
    Semicolon,
    LParen,
    RParen,
    LBrace,
    RBrace,
    // Keywords
    Function,
    Let,
    If,
    Else,
    True,
    False,
    Return,
}

const KEYWORDS: [(&'static str, Token); 7] = [
    ("fn", Token::Function),
    ("let", Token::Let),
    ("if", Token::If),
    ("else", Token::Else),
    ("true", Token::True),
    ("false", Token::False),
    ("return", Token::Return),
];

fn lookup_ident(ident: String) -> Token {
    match KEYWORDS.into_iter().find(|(kw, t)| kw == &ident) {
        Some((_, token)) => token,
        None => Token::Ident(ident),
    }
}

#[derive(Debug)]
pub struct Lexer {
    input: Vec<u8>,
    position: usize,
    read_position: usize,
    char: u8,
    pub column: usize,
    pub line: usize,
}

impl Lexer {
    pub fn new(input: String) -> Lexer {
        let mut lexer = Lexer {
            input: input.into_bytes(),
            position: 0,
            read_position: 0,
            char: b'\0',
            column: 1,
            line: 0,
        };
        lexer.read_char();
        lexer
    }

    pub fn next_token(&mut self) -> Token {
        self.skip_whitespace();
        let token = match self.char {
            b'=' => {
                if b'=' == self.peek_char() {
                    self.read_char();
                    Token::Equal
                } else {
                    Token::Assign
                }
            },
            b';' => Token::Semicolon,
            b'(' => Token::LParen,
            b')' => Token::RParen,
            b',' => Token::Comma,
            b'+' => Token::Plus,
            b'{' => Token::LBrace,
            b'}' => Token::RBrace,
            b'\0' => Token::EOF,
            b'>' => Token::RChevron,
            b'<' => Token::LChevron,
            b'!' => {
                if b'=' == self.peek_char() {
                    self.read_char();
                    Token::NotEqual
                } else {
                    Token::Bang
                }
            }
            b'/' => Token::Slash,
            b'-' => Token::Minus,
            b'*' => Token::Asterisk,
            b'a'..=b'z' | b'A'..=b'Z' | b'_' => {
                let literal = self.read_identifier();
                return lookup_ident(literal); // May fail because we may read after this
            }
            b'0'..=b'9' => {
                let literal = self.read_digit();
                return Token::Integer(literal);
            }
            _ => Token::Illegal,
        };
        self.read_char();
        token
    }

    fn skip_whitespace(&mut self) {
        while self.char.is_ascii_whitespace() {
            self.read_char()
        }
    }

    fn peek_char(&self) -> u8 {
        if self.read_position >= self.input.len() {
            b'\0'
        } else {
            self.input[self.read_position]
        }
    }

    fn read_char(&mut self) {
        if self.read_position >= self.input.len() {
            self.char = b'\0';
        } else {
            self.char = self.input[self.read_position];
        }

        self.column += 1;

        if self.char == b'\n' {
            self.column = 1;
            self.line += 1;
        }

        self.position = self.read_position;
        self.read_position += 1;
    }

    fn read_identifier(&mut self) -> String {
        let position = self.position;
        while self.char.is_ascii_alphabetic() {
            self.read_char();
        }
        String::from_utf8(self.input[position..self.position].to_vec())
            .expect("What do you mean this is not convertible to a string")
    }

    fn read_digit(&mut self) -> String {
        let position = self.position;
        while self.char.is_ascii_digit() {
            self.read_char();
        }
        String::from_utf8(self.input[position..self.position].to_vec())
            .expect("What do you mean this is not convertible to a string")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_next_token() {
        let input = "let five = 5;
let ten = 10;

let add = fn(x, y) {
  x + y;
};

let result = add(five, ten);
!-/*5;
5 < 10 > 5;
if (5 < 10) {
    return true;
} else {
    return false;
}

10 == 10;
10 != 9;
";

        let expected = [
            Token::Let,
            Token::Ident("five".into()),
            Token::Assign,
            Token::Integer("5".into()),
            Token::Semicolon,
            Token::Let,
            Token::Ident("ten".into()),
            Token::Assign,
            Token::Integer("10".into()),
            Token::Semicolon,
            Token::Let,
            Token::Ident("add".into()),
            Token::Assign,
            Token::Function,
            Token::LParen,
            Token::Ident("x".into()),
            Token::Comma,
            Token::Ident("y".into()),
            Token::RParen,
            Token::LBrace,
            Token::Ident("x".into()),
            Token::Plus,
            Token::Ident("y".into()),
            Token::Semicolon,
            Token::RBrace,
            Token::Semicolon,
            Token::Let,
            Token::Ident("result".into()),
            Token::Assign,
            Token::Ident("add".into()),
            Token::LParen,
            Token::Ident("five".into()),
            Token::Comma,
            Token::Ident("ten".into()),
            Token::RParen,
            Token::Semicolon,
            Token::Bang,
            Token::Minus,
            Token::Slash,
            Token::Asterisk,
            Token::Integer("5".into()),
            Token::Semicolon,
            Token::Integer("5".into()),
            Token::LChevron,
            Token::Integer("10".into()),
            Token::RChevron,
            Token::Integer("5".into()),
            Token::Semicolon,
            Token::If,
            Token::LParen,
            Token::Integer("5".into()),
            Token::LChevron,
            Token::Integer("10".into()),
            Token::RParen,
            Token::LBrace,
            Token::Return,
            Token::True,
            Token::Semicolon,
            Token::RBrace,
            Token::Else,
            Token::LBrace,
            Token::Return,
            Token::False,
            Token::Semicolon,
            Token::RBrace,
            Token::Integer("10".into()),
            Token::Equal,
            Token::Integer("10".into()),
            Token::Semicolon,
            Token::Integer("10".into()),
            Token::NotEqual,
            Token::Integer("9".into()),
            Token::Semicolon,
        ];

        let mut l = Lexer::new(input.to_string());

        for tt in expected {
            let tok = l.next_token();
            assert_eq!(tok, tt);
        }
    }
}
